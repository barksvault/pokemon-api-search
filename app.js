const express = require("express");
const cors = require("cors");
const connect = require("./utils/mongo").connect;
const collection = require("./utils/mongo").collection;
const app = express();
const port = process.env.PORT || 8080;

app.use(cors());

app.get("/", (req, res) => {
  res.end("My amazing API");
});

app.get("/pokemons", function(req, res) {
  const pokeCollection = collection("pokemons");
  pokeCollection
    .find({
      name: {
        $regex: req.query.search,
        $options: "i"
      }
    })
    .toArray()
    .then(pokemons => {
      res.json(pokemons);
    });
});

connect().then(() => {
  console.log("Database connected");
  app.listen(port, function() {
    console.log(`Express server listens on http://localhost:${port}`);
  });
});
